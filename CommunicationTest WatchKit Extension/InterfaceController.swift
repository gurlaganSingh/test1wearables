//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {

    
    // MARK: Outlets
    
    var pokemonHunger = 0
    var pokemonHealth = 100
    var timerCount = Timer()
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    
    
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        let messageBody = message["pokemon"] as! String
//        let name : String = message["name"] as! String
//        let name1 : String = message["name1"] as! String
 //       self.outputLabel.setText("HP: \(name)  Hunger:\(name1)")
        messageLabel.setText(messageBody)
        
        if(messageBody == "pikachu")
        {
            pokemonImageView.setImage(UIImage(named:"pikachu"))
        }
        else if(messageBody == "caterPie")
        {
            pokemonImageView.setImage(UIImage(named:"caterpie"))
        }
 }

 // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        beginCounter()
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to watch")
            WCSession.default.sendMessage(
                ["name" : "Pritesh"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
    }
    
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func nameButtonPressed() {
        print("name button pressed")
      
        let suggestedResponses = ["albert", "gurlagan"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) { (results) in
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.nameLabel.setText(userResponse)
            }
        }

    }
    
    func beginCounter(){
        timerCount = Timer.scheduledTimer(timeInterval: 5.0 , target: self, selector: #selector(countDownTimer), userInfo: nil, repeats: true)
    }
    
    @objc func countDownTimer(){
        if(self.pokemonHealth > 0 && self.pokemonHealth <= 100){
        
        if(self.pokemonHunger < 80){
        self.pokemonHunger = self.pokemonHunger + 10
        self.outputLabel.setText("HP: 100  Hunger:\(self.pokemonHunger)")
                    }
        else if( self.pokemonHunger >= 80){
        self.pokemonHunger = self.pokemonHunger + 10
        self.pokemonHealth = self.pokemonHealth - 5
        self.outputLabel.setText("HP: \(self.pokemonHealth)  Hunger:\(self.pokemonHunger)")
                    }
                }
        if(self.pokemonHealth <= 0){
        self.messageLabel.setText("Pokemon died")
        self.outputLabel.setText("HP: \(self.pokemonHealth)  Hunger:\(self.pokemonHunger)")
                }
    
//        if (WCSession.default.isReachable) {
//            let message1 = [ "pokemon": "update Received","hunger": pokemonHunger, "health" : pokemonHealth] as [String : Any]
//            WCSession.default.sendMessage(message1, replyHandler: nil)
//        }
        else {
            //  print("update PHONE: Cannot reach watch")
          //  outputLabel.insertText("\n update Cannot reach watch")
        }
        
    }

    @IBAction func startButtonPressed() {
        print("Start button pressed")
        
        
    }
    
    @IBAction func feedButtonPressed() {
        print("Feed button pressed")
        
        self.pokemonHunger = self.pokemonHunger - 12
        self.outputLabel.setText("HP: \(self.pokemonHealth)  Hunger:\(self.pokemonHunger)")
  }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to watch")
            WCSession.default.sendMessage(
                ["pokemonHealth" : "\(self.pokemonHealth)", "pokemonHunger" : "\(self.pokemonHunger)"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
        
    }
    
}
